const express = require("express");
const cors = require("cors");

const { uuid } = require("uuidv4");

const app = express();

app.use(express.json());
app.use(cors());

const repositories = [];

app.get("/repositories", (request, response) => {
  return response.json(repositories);
});

app.post("/repositories", (request, response) => {
  const { title, url, techs, likes } = request.body;

  const repository = {
    id: uuid(),
    title,
    url,
    techs,
    likes,
  };

  repositories.push(repository);

  return response.status(200).json({
    message: "Repositorio Criado",
  });
});

app.put("/repositories/:id", (request, response) => {
  const { id } = request.params;
  const { title, url, techs, likes } = request.body;

  const repositoryIndex = repositories.findIndex(
    (repository) => repository.id === id
  );

  if (repositoryIndex < 0) {
    return response.status(401).json({
      error: "Repositorio nao encontrado",
    });
  }

  const repositoryChangedObj = {
    id,
    title,
    url,
    techs,
    likes,
  };

  repositories[repositoryIndex] = repositoryChangedObj;

  response.status(200).json({
    message: "Informacoes do repositorio foram atualizadas",
    newData: repositories,
  });
});

app.delete("/repositories/:id", (request, response) => {
  const { id } = request.params;

  const repositoryIndex = repositories.findIndex(
    (repository) => repository.id === id
  );

  if (repositoryIndex < 0) {
    return response.status(401).json({
      error: "Repositorio nao encontrado",
    });
  }

  repositories.splice(repositoryIndex);

  response.status(201).json({
    message: "Repositorio deletado com sucesso",
  });
});

app.post("/repositories/:id/like", (request, response) => {
  const { id } = request.params;
  const repositoryIndex = repositories.findIndex(
    (repository) => repository.id === id
  );

  repositories[repositoryIndex].likes++;

  response.status(201).json({
    message: `O repositorio ganhou like`,
    changedData: repositories[repositoryIndex],
  });
});

module.exports = app;
