## Backend - NodeJS - Challenge 1

<p align="center"> Concepts covered in module 1 on NodeJS </p>

<hr>

## Autor

| [<img src="https://avatars2.githubusercontent.com/u/12503997?v=4" width="75px;"/>](https://github.com/emersonjds) |
| :---------------------------------------------------------------------------------------------------------------: |


| [Emerson Silva](https://github.com/emersonjds)

## Techs

- [x] Node.js
- [x] Express
- [x] Jest
- [x] uuidv4
- [x] nodemon

## Usage

1. Run `yarn` for install dependencies.<br />
2. Run `yarn dev` and access `http://localhost:3333`.<br />

## Methods Implementeds

1. [GET]`/repositories`
1. [POST]`/repositories`
1. [PUT]`/repositories/:id`
1. [DELETE]`/repositories/:id`
1. [POST]`/repositories/:id/like`

## License

[GPL-3.0](emersonjds@fsf.com) © Emerson Silva
